import React from 'react';

type MainLayoutProps = {
  children: React.ReactNode;
};

export const MainLayout = ({ children }: MainLayoutProps) => {
  return (
    <main>
      <div className='container'>{children}</div>
    </main>
  );
};
