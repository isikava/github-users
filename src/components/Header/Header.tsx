import React, { FormEvent, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import './Header.css';
import { Breadcrumbs } from '../Breadcrumbs/Breadcrumbs';

export const Header = () => {
  const [searchValue, setSearchValue] = useState('');
  const navigate = useNavigate();

  const onSubmit = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (!searchValue.trim().length) {
      return;
    }

    navigate(`/search?query=${searchValue}`);
  };

  return (
    <header className='header'>
      <div className='container header__container'>
        <nav className='header__navigation'>
          <Breadcrumbs />
        </nav>

        <div className='header__search'>
          <form className='header__search-form' onSubmit={onSubmit}>
            <input
              type='search'
              className='header__search-input'
              placeholder='Поиск пользователя'
              value={searchValue}
              onChange={(event) => setSearchValue(event.currentTarget.value)}
            />
            <button type='submit' className='header__search-button'>
              Найти
            </button>
          </form>
        </div>
      </div>
    </header>
  );
};
