import React from 'react';
import { NavLink } from 'react-router-dom';
import useBreadcrumbs, {
  BreadcrumbComponentType,
  BreadcrumbMatch,
  BreadcrumbsRoute,
} from 'use-react-router-breadcrumbs';
import './Breadcrumbs.css';

type DynamicUserBreadcrumbProps = {
  match: BreadcrumbMatch<'id'>;
};

const DynamicUserBreadcrumb: BreadcrumbComponentType<'id'> = ({ match }: DynamicUserBreadcrumbProps) => {
  return <span className={'header__navigation-link--user'}>{match.params.id}</span>;
};

const routes: BreadcrumbsRoute[] = [
  { path: '/', breadcrumb: 'Пользователи гитхаба' },
  { path: '/users/:id', breadcrumb: DynamicUserBreadcrumb },
  { path: '/search', breadcrumb: 'поиск' },
];

export const Breadcrumbs = () => {
  const breadcrumbs = useBreadcrumbs(routes, {
    excludePaths: ['/users'],
  });

  return (
    <ul className='header__navigation-list'>
      {breadcrumbs.map(({ match, breadcrumb }) => (
        <li className='header__navigation-list-item' key={match.pathname}>
          <NavLink className='header__navigation-link' to={match.pathname}>
            {breadcrumb}
          </NavLink>
        </li>
      ))}
    </ul>
  );
};
