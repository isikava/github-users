import React, { useEffect } from 'react';
import { UserProfilePage } from '../../pages/UserProfilePage/UserProfilePage';
import { UsersPage } from '../../pages/UsersPage/UsersPage';
import { UsersSearchPage } from '../../pages/UsersSearchPage/UsersSearchPage';
import { Navigate, Route, Routes, useLocation } from 'react-router-dom';
import { Header } from '../Header/Header';

export const App = () => {
  const { pathname } = useLocation();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pathname]);

  return (
    <>
      <Header />
      <Routes>
        <Route path='/' element={<UsersPage />} />
        <Route path='/users' element={<UsersPage />} />
        <Route path='/users/:id' element={<UserProfilePage />} />
        <Route path='/search' element={<UsersSearchPage />} />
        <Route path='*' element={<Navigate to='/' replace={true} />} />
      </Routes>
    </>
  );
};
