import React from 'react';
import { Link } from 'react-router-dom';
import './UsersList.css';
import { User } from '../../types';

type UsersListProps = {
  users: User[];
};

export const UsersList = ({ users }: UsersListProps) => {
  return (
    <div className='users-list'>
      {users.map((u) => (
        <section className='users-list__item' key={u.id}>
          <div className='users-list__image-container'>
            <img className='users-list__image' src={u.avatar_url} alt={`${u.login} profile photo`} />
          </div>
          <div className='users-list__content'>
            <h2 className='users-list__title'>
              <Link to={`/users/${u.login}`} className='link'>
                {u.login}
              </Link>
              {/*, 15 репозиториев*/}
            </h2>
            {/*<p className='users-list__text'>Название организации</p>*/}
          </div>
        </section>
      ))}
    </div>
  );
};
