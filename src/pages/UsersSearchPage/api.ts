import { axios } from '../../lib/axios';
import { UsersSearch } from '../../types';

export const getUsersSearch = (query: string): Promise<UsersSearch> => {
  return axios.get(`/search/users?q=${query}`);
};
