import React, { useEffect, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import { UsersList } from '../../components/UsersList/UsersList';
import { MainLayout } from '../../components/Layout/MainLayout';
import { User } from '../../types';
import { getUsersSearch } from './api';

export const UsersSearchPage = () => {
  const [users, setUsers] = useState<User[] | null>(null);
  const [error, setError] = useState('');
  const [searchParams] = useSearchParams();
  const query = searchParams.get('query') || '';

  useEffect(() => {
    const getData = async () => {
      try {
        const { items } = await getUsersSearch(query);
        setUsers(items);
      } catch (err: any) {
        setError(err.message as string);
      }
    };

    getData();
  }, [query]);

  if (error) {
    return <MainLayout>{error}</MainLayout>;
  }

  if (users === null) {
    return <MainLayout>Loading...</MainLayout>;
  }

  return (
    <MainLayout>
      <h1 className='title'>Пользователи по запросу {query}</h1>
      {users.length > 0 ? <UsersList users={users} /> : <h2 className='title'>Пользователей не найдено</h2>}
    </MainLayout>
  );
};
