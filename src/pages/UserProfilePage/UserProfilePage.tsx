import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import './UserProfilePage.css';
import { MainLayout } from '../../components/Layout/MainLayout';
import { getUser, getUserRepos } from './api';
import { UserProfile, UserRepository } from '../../types';

export const UserProfilePage = () => {
  const { id } = useParams<{ id: string }>();
  const [user, setUser] = useState<UserProfile | null>(null);
  const [repos, setRepos] = useState<UserRepository[]>([]);
  const [error, setError] = useState('');

  useEffect(() => {
    const getData = async (username: string) => {
      try {
        const userData = await getUser(username);
        const reposData = await getUserRepos(userData.repos_url);

        setUser(userData);
        setRepos(reposData);
      } catch (err: any) {
        setError(('Ошибка загрузки пользователя. ' + err.message) as string);
      }
    };

    if (id) {
      getData(id);
    }
  }, []);

  if (error) {
    return <MainLayout>{error}</MainLayout>;
  }

  if (user === null) {
    return <MainLayout>Loading...</MainLayout>;
  }

  return (
    <MainLayout>
      <section className='user-profile'>
        <div className='user-profile__image-container'>
          <img className='user-profile__image' src={user.avatar_url} alt={`${user.login} profile photo`} />
        </div>
        <div className='user-profile__content'>
          <h1 className='user-profile__title'>
            {user.name && user.name + ', '} <span className='user-profile__accent'>{user.login}</span>
          </h1>
          <p className='user-profile__text'>
            <span className='user-profile__accent'>{user.followers}</span> Подписчиков ·{' '}
            <span className='user-profile__accent'>{user.following}</span> Подписок ·{' '}
            {user.blog && (
              <a href={user.blog} target='_blank' rel='noreferrer' className='link'>
                {user.blog}
              </a>
            )}
          </p>
        </div>
      </section>

      <section className='repository-list'>
        {repos.length > 0 ? (
          <>
            <div className='repository-list__header'>
              <h2 className='repository-list__title'>Репозитории</h2>
              <a href={user.html_url + '?tab=repositories'} className='link' target='_blank' rel='noreferrer'>
                Все репозитории
              </a>
            </div>

            <div className='repository-list__container'>
              {repos.map((r) => (
                <section className='repository-list__item' key={r.id}>
                  <h3 className='repository-list__item-title'>
                    <a href={r.html_url} target='_blank' rel='noreferrer' className='link'>
                      {r.name}
                    </a>
                  </h3>
                  <p className='repository-list__item-text'>{r.description}</p>
                </section>
              ))}
            </div>
          </>
        ) : (
          <h2 className='repository-list__title'>У {user.login} нет публичных репозиториев</h2>
        )}
      </section>
    </MainLayout>
  );
};
