import { axios } from '../../lib/axios';
import { UserProfile, UserRepository } from '../../types';

export const getUser = (username: string): Promise<UserProfile> => {
  return axios.get(`/users/${username}`);
};

export const getUserRepos = (url: string): Promise<UserRepository[]> => {
  return axios.get(url);
};
