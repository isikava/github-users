import { axios } from '../../lib/axios';
import { User } from '../../types';

export const getUsers = (): Promise<User[]> => {
  return axios.get('/users');
};
