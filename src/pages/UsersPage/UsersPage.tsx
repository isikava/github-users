import React, { useEffect, useState } from 'react';
import { UsersList } from '../../components/UsersList/UsersList';
import { MainLayout } from '../../components/Layout/MainLayout';
import { getUsers } from './api';
import { User } from '../../types';

export const UsersPage = () => {
  const [users, setUsers] = useState<User[]>([]);
  useEffect(() => {
    const getData = async () => {
      const data = await getUsers();
      setUsers(data);
    };

    getData();
  }, []);

  return (
    <MainLayout>
      <UsersList users={users} />
    </MainLayout>
  );
};
