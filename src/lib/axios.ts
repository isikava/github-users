import Axios from 'axios';

const API_URL = 'https://api.github.com';

export const axios = Axios.create({
  baseURL: API_URL,
});

axios.interceptors.response.use(
  (response) => {
    return response.data;
  },
  (error) => {
    // const message = error.response?.data?.message || error.message;
    // console.warn(message);

    return Promise.reject(error);
  }
);
