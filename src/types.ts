export interface User {
  id: number;
  login: string;
  avatar_url: string;
  html_url: string;
  repos_url: string;
}

export interface UserProfile {
  id: number;
  login: string;
  name: string | null;
  blog: string | null;
  avatar_url: string;
  html_url: string;
  repos_url: string;
  followers: number;
  following: number;
}

export interface UserRepository {
  id: number;
  name: string;
  description: string | null;
  html_url: string;
}

export interface UsersSearch {
  total_count: number;
  incomplete_results: boolean;
  items: User[];
}
